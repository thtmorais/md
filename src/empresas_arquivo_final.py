#!/usr/bin/env python

import numpy as np
import pandas as pd
from unicodedata import normalize
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
nltk.download('punkt')
from nltk.tokenize import word_tokenize
from wordcloud import WordCloud
from sklearn.cluster import KMeans
from sklearn import metrics
from sklearn.metrics import silhouette_score
from collections import defaultdict
from sklearn import metrics
from time import time
from sklearn.decomposition import TruncatedSVD
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import Normalizer
from sklearn.cluster import AgglomerativeClustering
import random
import os
import json

# variables globais
cnaes_secundarios = []
dados = []
evaluations = []
evaluations_std = []
tfcv = None

number_cities = 0

stop_words_pt = stopwords.words('portuguese')

# read json cities.json
def read_cities():
  cities = []
  f = open(os.getcwd() + "/data/cities.json")
  dataJson = json.load(f)
  for key, value in dataJson.items():
      cities.append({'codigo_municipio': key, 'municipio':value})
  f.close()
  return cities

def validar_cnae(cnaes, elemento):
    elemento = str(elemento)
    return  elemento[:2] in cnaes[0] or elemento[:3] in cnaes[1]   

def remover_acentos(txt):
    txt = str(txt)
    txt = txt.replace("/", "").replace(".", "").replace(",", "").replace("-", "")
    return normalize('NFKD', txt).encode('ASCII', 'ignore').decode('ASCII')

def remove_stopwords(lista):
  cnaes_sem_stopwords = []

  for cnae in lista:
      text_tokens = word_tokenize(cnae)
      cnaes_sem_stopwords.append([word for word in text_tokens if not word in stopwords.words()])
  return cnaes_sem_stopwords

def fit_and_evaluate(km, X, name=None, n_runs=5):
    name = km.__class__.__name__ if name is None else name

    labels = km.labels_

    train_times = []
    scores = defaultdict(list)
    for seed in range(n_runs):
        km.set_params(random_state=seed)
        t0 = time()
        km.fit(X)
        train_times.append(time() - t0)
        scores["Homogeneity"].append(metrics.homogeneity_score(labels, km.labels_))
        scores["Completeness"].append(metrics.completeness_score(labels, km.labels_))
        scores["V-measure"].append(metrics.v_measure_score(labels, km.labels_))
        scores["Adjusted Rand-Index"].append(
            metrics.adjusted_rand_score(labels, km.labels_)
        )
        scores["Silhouette Coefficient"].append(
            metrics.silhouette_score(X, km.labels_, sample_size=2000)
        )
    train_times = np.asarray(train_times)

    print(f"clustering done in {train_times.mean():.2f} ± {train_times.std():.2f} s ")
    evaluation = {
        "estimator": name,
        "train_time": train_times.mean(),
    }
    evaluation_std = {
        "estimator": name,
        "train_time": train_times.std(),
    }
    for score_name, score_values in scores.items():
        mean_score, std_score = np.mean(score_values), np.std(score_values)
        print(f"{score_name}: {mean_score:.3f} ± {std_score:.3f}")
        evaluation[score_name] = mean_score
        evaluation_std[score_name] = std_score
    evaluations.append(evaluation)
    evaluations_std.append(evaluation_std)

def execute_kmeans_primary():
  dataset_completo = pd.read_csv((os.getcwd() + "/data/empresas_go.csv"), delimiter=";")
  # dataset_completo = pd.read_csv((os.getcwd() + "/data/empresas_go.csv"), encoding="UTF-8", sep=",")

  cnaes_energia = ['35', '19', '05', '06', '26','27']
  sub_cnaes_energia = ['091']

  dataset_completo = dataset_completo[[validar_cnae((cnaes_energia, sub_cnaes_energia), x) for x in dataset_completo['cnae_fiscal']]]

  cnae = []
  index = 0
  for i in dataset_completo['cnaes_secundarios']:
      cnae.append(remover_acentos(dataset_completo.iloc[index][30]))
      index += 1

  index = 0

  dataset_completo['cnaes_secundarios'] = cnae

  cnae = []
  index = 0
  for i in dataset_completo['cnae_fiscal_descricao']:
      cnae.append(remover_acentos(dataset_completo.iloc[index][29]))
      index += 1

  index = 0

  dataset_completo['cnae_fiscal_descricao'] = cnae

  for i in dataset_completo['cnaes_secundarios']:
    vec = []
    if i != '{}':
      a = i.split("\"")
      for x in range(len(a)):
        if x % 4 == 3:
          vec.append(a[x])
    else:
      vec.append("")
    cnaes_secundarios.append(vec)

  for i in range(len(dataset_completo['razao_social'])):
    text = remover_acentos(dataset_completo.iloc[i][3]) + " - " + remover_acentos(dataset_completo.iloc[i][0]) + " - "
    text = text + dataset_completo.iloc[i][29] + " "
    for n in cnaes_secundarios[i]:
      text = text + n + " "
    dados.append(text)

  global tfcv
  tfcv = TfidfVectorizer(lowercase=True, stop_words=stop_words_pt, min_df=2)

  X = tfcv.fit_transform(dados)
  tfcv.get_feature_names_out()
  
  return dataset_completo

def execute(dataset_completo, city_code, city_name):
  dataset_anapolino = dataset_completo[dataset_completo['municipio'] == city_name]

  dados_anapolis = []

  if dataset_anapolino.empty: 
    return
  
  for i in range(len(dataset_anapolino['razao_social'])):
    text = remover_acentos(dataset_anapolino.iloc[i][3]) + " - " + remover_acentos(dataset_anapolino.iloc[i][0]).rjust(14, '0') + " - "
    text = text + dataset_anapolino.iloc[i][29] + " "
    for n in cnaes_secundarios[i]:
      text = text + n + " "
    dados_anapolis.append(text)

  #fit_anapolis = tfcv.fit(dados)
  X_anapolis = tfcv.transform(dados_anapolis)

  scores_kmeans = dict()
  for i in range(2, X_anapolis.shape[0]-1):
    if i > 7:
      break
    kmeans = KMeans(
      n_clusters=i,
      n_init=5
    ).fit(X_anapolis)
    
    silhouette_anapolis = silhouette_score(X_anapolis, kmeans.labels_)
    scores_kmeans[i] = silhouette_anapolis

  frame_anapolis = pd.DataFrame(scores_kmeans.items(), columns=['k', 'Silhouette'])

  scores_random = dict()
  label = []
  for i in range(2, X_anapolis.shape[0]-1):
    if i > 7:
      break
    for j in range(X_anapolis.shape[0]):
      label.append(random.randint(0, i-1))
    
    silhouette_random = silhouette_score(X_anapolis, label)
    scores_random[i] = silhouette_random
    
    label = []

  frame_random = pd.DataFrame(scores_random.items(), columns=['k', 'Silhouette'])

  data_complete = pd.DataFrame(columns=['num_clusters', 'kmeans', 'random'])

  data_complete['num_clusters'] = frame_anapolis['k']
  data_complete['kmeans'] = frame_anapolis['Silhouette']
  data_complete['random'] = frame_random['Silhouette']

  data_complete['difference'] = data_complete['kmeans']-data_complete['random']
  data_complete['difference'] = pd.to_numeric(data_complete['difference'])

  if  data_complete.empty:
    return
    
  max_value = data_complete[['difference']].idxmax()

  max_value = max_value[0]

  kmeans_anapolis = KMeans(
      n_clusters=max_value,
      n_init=5
  ).fit(X_anapolis)

  cluster_ids_anapolis, cluster_sizes_anapolis = np.unique(kmeans_anapolis.labels_, return_counts=True)
  print(f"Número de elementos em cada cluster: {cluster_sizes_anapolis}")

  len(cluster_sizes_anapolis)

  silhouette_anapolis = silhouette_score(X_anapolis, kmeans_anapolis.labels_)
  print("Silhoutte Score = {:.3f}\n".format(silhouette_anapolis))

  lsa_anapolis = make_pipeline(TruncatedSVD(n_components=100), Normalizer(copy=False))
  t0 = time()
  X_lsa_anapolis = lsa_anapolis.fit_transform(X_anapolis)
  explained_variance_anapolis = lsa_anapolis[0].explained_variance_ratio_.sum()

  print(f"LSA done in {time() - t0:.3f} s")
  print(f"Explained variance of the SVD step: {explained_variance_anapolis * 100:.1f}%")

  num_clusters = max_value

  kmeans_anapolis = KMeans(
      n_clusters=num_clusters,
      max_iter=100,
      n_init=1,
  ).fit(X_lsa_anapolis)

  fit_and_evaluate(kmeans_anapolis, X_lsa_anapolis, name="KMeans\nwith LSA on tf-idf vectors")

  cluster_ids_anapolis, cluster_sizes_anapolis = np.unique(kmeans_anapolis.labels_, return_counts=True)

  dados_list_anapolis = []
  count_city = 0
  # clusters_anapolis = []
  for i in range(len(cluster_sizes_anapolis)):
    # vec = []
    vec_list = []
    for j in np.argwhere(kmeans_anapolis.labels_ == i).reshape(-1):
        # vec.append(dados_anapolis[j])
        
        dados_vec = str(dados_anapolis[j]).split("-")
        vec_list.append({
          "cnpj" : dados_vec[1],
          "razao_social" : dados_vec[0],
        })
        count_city = count_city + 1
    # clusters_anapolis.append(vec)
    dados_list_anapolis.append({
      "cluster" : i,
      "empresas" : vec_list
    })

  original_space_centroids_anapolis = lsa_anapolis[0].inverse_transform(kmeans_anapolis.cluster_centers_)
  order_centroids_anapolis = original_space_centroids_anapolis.argsort()[:, ::-1]
  terms_anapolis = tfcv.get_feature_names_out()

  top_5_idx_anapolis = np.argsort(cluster_sizes_anapolis)[-5:]
  top_5_values_anapolis = [cluster_sizes_anapolis[i] for i in top_5_idx_anapolis]

  lista_anapolis=[]

  if not os.path.isdir(os.getcwd() + "/../public/img/" + city_code):
    os.makedirs(os.getcwd() + "/../public/img/" + city_code)
  
  if not os.path.isdir(os.getcwd() + "/../public/json/" + city_code):
    os.makedirs(os.getcwd() + "/../public/json/" + city_code)

  cont = 0
  for i in top_5_idx_anapolis[::-1]:
    words = ""
    for ind in order_centroids_anapolis[i, :40]:
      words = words + " " + terms_anapolis[ind]
    lista_anapolis.append(words)

    with open(os.getcwd() + "/../public/json/" + city_code + "/" + str(cont) + "_list.json", "w") as outfile:
      json.dump(dados_list_anapolis[dados_list_anapolis[i]['cluster']], outfile)


    wordcloud_anapolis = WordCloud().generate(lista_anapolis[cont])
    wordcloud_anapolis.to_file(os.getcwd() + "/../public/img/" + city_code + "/" + str(cont) + "_wordcloud.png")
    cont = cont + 1
  return count_city

dataset_completo_return = execute_kmeans_primary()
cities = read_cities()

for item in cities:
  try:
    count_cities = execute(dataset_completo_return, item['codigo_municipio'], item['municipio'])
    print(count_cities)
  except Exception as error:
    with open(os.getcwd() + "/../public/error.txt", "a") as outfile:
      outfile.write(str(error) + "\n")